<?php
/**
* @project    Atom-M CMS
* @package    Foto Model
* @url        https://atom-m.modos189.ru
*/


namespace FotoModule\ORM;

class FotoModel extends \OrmModel
{
    public $Table = 'foto';

    protected $RelatedEntities = array(
        'author' => array(
            'model' => 'Users',
            'type' => 'has_one',
            'foreignKey' => 'author_id',
        ),
        'category' => array( // Deprecated, because not supporting multiple categories
            'model' => 'FotoCategories',
            'type' => 'has_one',
            'foreignKey' => 'category_id',
        ),
        'categories' => array(
            'model' => 'FotoCategories',
            'type' => 'has_many',
            'foreignKey' => 'this.category_id',
        ),
    );



    public function getNextPrev($id)
    {
        $records = array('prev' => array(), 'next' => array());
        $prev = getDB()->select($this->Table, DB_FIRST, array('cond' => array('`id` < ' . $id), 'limit' => 1, 'order' => '`id` DESC'));
        if (!empty($prev[0])) $records['prev'] = new FotoEntity($prev[0]);
        $next = getDB()->select($this->Table, DB_FIRST, array('cond' => array('`id` > ' . $id), 'limit' => 1, 'order' => '`id`'));
        if (!empty($next[0])) $records['next'] = new FotoEntity($next[0]);


        return $records;
    }

    /**
     * @param $user_id
     * @return array|bool
     */
    function __getUserStatistic($user_id) {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        
        $user_id = intval($user_id);
        if ($user_id > 0) {
            $result = $this->getTotal(array('cond' => array('author_id' => $user_id)));
            if ($result) {
                $res = array(
                    'module' => $module,
                    'text' => __('foto',true,$module),
                    'count' => intval($result),
                    'url' => get_url("/$module/user/$user_id"),
                );

                return array($res);
            }
        }
        return false;
    }
    
    /**
     * Count of entries
     */
    function __getEntriesCount() {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        return array(
            'text' => __('foto',true,$module),
            'count' => $this->getTotal(),
        );
    }
    
    /**
     * Entries for search
     *
     */
    function __getQueriesForSearch() {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        return array(
            "SELECT `title` as `index`, `id` as `entity_id`, '$this->Table' as `entity_table`, '/view/' as `entity_view`, '$module' as `module`, `date` FROM `" .  getDB()->getFullTableName($this->Table) . "` WHERE `title` IS NOT NULL AND `title` <> ''",
        );
    }
    
    /**
     * Results for search
     *
     */
   /**
     * Results for search
     *
     */
    function __correctSearchResults($results) {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        
        $comments_id = array();
        $entities_id = array();
        
        $need_comments = \ACL::turnUser(array($module, 'view_comments'));
        $need_entities = \ACL::turnUser(array($module, 'view_materials'));
        if (!$need_comments && !$need_entities) {
            return array();
        }
        foreach ($results as $result) {
            if ($result->getEntity_table() == 'comments' && $need_comments) { // Comment
                $comments_id[] = $result->getEntity_id();
            } elseif ($need_entities) { // Title, text or tags material
                $entities_id[] = $result->getEntity_id();
            }
        }
        $comments_id = array_unique($comments_id);
        $entities_id = array_unique($entities_id);
        
        $this->bindModel('categories');
        $items = count($comments_id) || count($entities_id) ? $this->getCollection(array(
            (count($entities_id) ? '`id` IN (' . implode(',', $entities_id) . ')' : '')
            . (count($comments_id) && count($entities_id) ? ' OR ' : '')
            . (count($comments_id) ? '`id` IN (SELECT `entity_id` FROM ' . getDB()->getFullTableName('comments') . ' WHERE `id` IN (' . implode(',', $comments_id) . '))' : '')
        )) : array();
        $entities = array();
        foreach ($items as $entity) {
            $entities[$entity->getId()] = $entity;
        }
        
        $commentsModel = \OrmManager::getModelInstance('comments');
        $items = count($comments_id) ? $commentsModel->getCollection(array('`id` in (' . implode(',', $comments_id) . ')')) : array();
        $comments = array();
        foreach ($items as $comment) {
            $comments[$comment->getId()] = $comment;
        }
        
        $usersModel = \OrmManager::getModelInstance('users');
        $items = count($comments_id) || count($entities_id) ? $usersModel->getCollection(array(
            (count($entities_id) ? '`id` IN (SELECT `author_id` FROM ' . getDB()->getFullTableName($this->Table) . ' WHERE `id` IN (' . implode(',', $entities_id) . '))' : '')
            . (count($comments_id) && count($entities_id) ? ' OR ' : '')
            . (count($comments_id) ? '`id` IN (SELECT `user_id` FROM ' . getDB()->getFullTableName('comments') . ' WHERE `id` IN (' . implode(',', $comments_id) . '))' : '')
        )) : array();
        $users = array();
        foreach ($items as $user) {
            $users[$user->getId()] = $user;
        }
        
        foreach ($results as $index => &$result) {
            if (($result->getEntity_table() == 'comments' && !$need_comments) ||
                ($result->getEntity_table() != 'comments' && !$need_entities)) {
                unset($results[$key]);
            }
            $result->setEntry_url(get_url($result->getModule() . $result->getEntity_view() . $result->getEntity_id()));
            $entity = isset($entities[$result->getEntity_id()]) ? $entities[$result->getEntity_id()] : null;
            if ($entity) {
                $result->setAuthor(isset($users[$entity->getAuthor_id()]) ? $users[$entity->getAuthor_id()] : null);
                if ($result->getEntity_table() == 'comments') {
                    $comment = isset($comments[$result->getEntity_id()]) ? $comments[$result->getEntity_id()] : null;
                    $result->setAuthor($comment && isset($users[$comment->getUser_id()]) ? $users[$comment->getUser_id()] : null);
                }
                $entity_categories = $entity->getCategories();
                if (count($entity_categories)) {
                    foreach ($entity_categories as $entity_category) {
                        $entity_category->setUrl(get_url($result->getModule() . '/category/' . $entity_category->getId()));
                    }

                    // New marker contains array with all categories
                    $result->setCategories($entity_categories);

                    // Old markers are based on first category
                    $entity->setCategory_title($entity_categories[0]->getTitle());
                    $entity->setCategory_url($entity_categories[0]->getUrl());
                }

                if ($entity->getTags()) {
                    $result->setTags(atrim(explode(',', $entity->getTags())));
                }

                $result->setTitle($entity->getTitle());
                $result->setViews($entity->getViews());
                $result->setComments($entity->getComments());
            }
        }
        return $results;
    }
    
    /**
     * List of links for Sitemap
     *
     */
    function __getSitemapList($host) {
        $module = \OrmManager::getModuleFromClassname(__CLASS__);
        $urls = array();
        
        $entities = getDB()->select($this->Table, DB_ALL, array());

        if (count($entities) > 0) {
            foreach ($entities as $entity) {
                $urls[] = "$host$module/view/" . $entity['id'];
            }
        }
        return $urls;
    }
}