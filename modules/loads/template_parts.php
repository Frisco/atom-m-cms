<?php
return array(
    'addform.html.twig'  => __('Add form'),
    'main.html.twig'     => __('Layout'),
    'editform.html.twig' => __('Edit form'),
    'material.html.twig' => __('Material view'),
    'list.html.twig'     => __('List of materials'),
);
