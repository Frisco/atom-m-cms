<?php

$json_data = array(
    'dates' => array(),
    'views' => array(),
    'hosts' => array()
);

foreach($all_stats as $day) {
    $json_data['dates'][] = date("m/d/Y",strtotime($day->getDate()));
    $json_data['views'][] = (int)$day->getViews();
    $json_data['hosts'][] = (int)$day->getVisits();
}


ob_start();
?>
<script type="text/javascript">
    function plotTimeAdapt(data) {
        for(var i=0; i<data.length; i++) {
            data[i] = (new Date(data[i])).toDateString();
        }
        return data;
    };
    $(document).ready(function(){
        
        var Stats_data = <?php echo json_encode($json_data); ?>;
        $('#Views_hosts').highcharts({
            chart: {
                type: 'areaspline',
                height: 300
            },
            title: {
                text: '<?php echo __('Views and hosts',false,'statistics') ?>',
            },
            xAxis: {
                categories: plotTimeAdapt(Stats_data.dates)
            },
            yAxis: {
                title: false
            },
            tooltip: {
                shared: true,
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.5
                }
            },
            series: [{
                name: '<?php echo __('Watchers',false,'statistics') ?>',
                data: Stats_data.views
            }, {
                name: '<?php echo __('Hosts',false,'statistics') ?>',
                data: Stats_data.hosts
            }],
            credits: false
        });
    });
</script>
<div id="Views_hosts"></div>
<?php
return ob_get_clean();
?>