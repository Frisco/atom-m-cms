<?php
/**
* @project    Atom-M CMS
* @package    ForumCat Model
* @url        https://atom-m.modos189.ru
*/


namespace ForumModule\ORM;

class ForumCatModel extends \OrmModel
{
    public $Table = 'forum_cat';

    protected $RelatedEntities = array(
        'forums' => array(
            'model' => 'Forum',
            'type' => 'has_many',
            'foreignKey' => 'in_cat',
          ),
    );


}