<?php
/**
* @project    Atom-M CMS
* @package    Polls Model
* @url        https://atom-m.modos189.ru
*/


namespace ForumModule\ORM;

class ForumPollsModel extends \OrmModel
{
    public $Table = 'polls';

    protected $RelatedEntities;
    
    public function deleteByTheme($theme_id) {
        getDB()->delete($this->Table, array('theme_id' => $theme_id));
    }

}