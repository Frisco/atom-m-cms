<?php
return array(
    'addnewuserform.html.twig'      => __('Registration page',false,'users'),
    'viewrules.html.twig'           => __('View rules',false,'users'),
    'edituserform.html.twig'        => __('Edit user profile',false,'users'),
    'edituserformbyadmin.html.twig' => __('Edit user profile by admin',false,'users'),
    'main.html.twig'                => __('Layout'),
    'loginform.html.twig'           => __('Login form',false,'users'),
    'baned.html.twig'               => __('Ban page',false,'users'),
    'showuserinfo.html.twig'        => __('Profile info',false,'users'),
    'list.html.twig'                => __('Users list',false,'users'),
    'newpasswordform.html.twig'     => __('Password repair',false,'users'),
    'pm.html.twig'                  => __('PM nav',false,'users'),
    'pm_view.html.twig'             => __('PM view',false,'users'),
    'rating_tb.html.twig'           => __('Rating history',false,'users'),
    'warning_tb.html.twig'          => __('Warnings history',false,'users'),
);
