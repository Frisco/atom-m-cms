<?php
/**
* @project    Atom-M CMS
* @package    Messages Entity
* @url        https://atom-m.modos189.ru
*/


namespace UsersModule\ORM;

class UsersMessagesEntity extends \OrmEntity
{

    protected $id;
    protected $to_user;
    protected $from_user;
    protected $sendtime;
    protected $subject;
    protected $message;
    protected $id_rmv;
    protected $viewed;




    public function save()
    {
        $params = array(
            'to_user' => intval($this->to_user),
            'from_user' => intval($this->from_user),
            'sendtime' => $this->sendtime,
            'subject' => $this->subject,
            'message' => $this->message,
            'id_rmv' => intval($this->id_rmv),
            'viewed' => intval($this->viewed),
        );
        if ($this->id) $params['id'] = $this->id;

        return (getDB()->save('messages', $params));
    }



    public function delete()
    {
        getDB()->delete('messages', array('id' => $this->id));
    }


    public function getListKeys() {
        return array_keys(get_object_vars($this));
    }


    public function __getAPI() {
        if (
            \UserAuth::isGuest()
            ||
            !\ACL::turnUser(array('users', 'view_users'))
            ||
            (
            $this->to_user !== \UserAuth::getField('id')
            &&
            $this->from_user !== \UserAuth::getField('id')
            )
        )
            return array();


        return array(
            'id' => $this->id,
            'to_user' => $this->to_user,
            'from_user' => $this->from_user,
            'sendtime' => $this->sendtime,
            'subject' => $this->subject,
            'message' => $this->message,
            'id_rmv' => $this->id_rmv,
            'viewed' => $this->viewed,
        );
    }

}
