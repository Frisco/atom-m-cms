<?php

namespace UsersModule;

class EventsHandler {
    
    static $support_events = array(
        "add_admin_homecard",
        "new_user"
    );
    
    function add_admin_homecard($cards) {
        $cnt_usrs = getDB()->select('users', DB_COUNT);
        $groups_info = array();
        $users_groups = \ACL::getGroups();
        if (!empty($users_groups)) {
            foreach ($users_groups as $key => $group) {
                if ($key === 0)
                    continue;
                
                $groups_info[] = array(
                    "name" => $group['title'],
                    "y" => (int)(getDB()->select('users', DB_COUNT, array('cond' => array('status' => $key))))
                );
            }
        }
        
        
        $cards[] = array(
            "order" => 1000,
            "is_row" => false,
            "title" => false,
            "body" => include('admin/homecard.php')
        );
        return $cards;
    }
    
    function new_user($params) {
        $Cache = new \Cache('users', 'users');
        if ($Cache->check('cnt_registered_users')) {
            $Cache->remove('cnt_registered_users');
        }
    }
}