<?php
ob_start();
?>
<script type="text/javascript">
$(document).ready(function () {
    // Build the chart
    $('#usersPie').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: '<?php echo __('users',false,'users'); ?>'
        },
        subtitle: {
            text: '<?php echo __('All cnt users',false,'users') ?>: <?php echo $cnt_usrs ?>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}({point.percentage:.1f}%)</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: "<?php echo __('users',false,'users'); ?>",
            colorByPoint: true,
            data: <?php echo json_encode($groups_info) ?>
        }],
        credits: false
    });
});
</script>

<div id="usersPie"></div>

<?php
return ob_get_clean();
?>