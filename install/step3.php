<?php

if (basename(dirname(__FILE__)) != "install") {
    header('Location: /');
    die();
}

include_once '../sys/boot.php';

$installed_modules = \ModuleManager::getInstalledModules();
$new_modules = \ModuleManager::getNewModules();
$modules = array_merge($installed_modules, $new_modules);

if (isset($_POST['send'])) {
    $need_modules = isset($_POST['modules']) && is_array($_POST['modules']) ? $_POST['modules'] : array();

    foreach ($installed_modules as $module) {
        if (!in_array($module, array('home', 'users')) && !isset($need_modules[$module])) {
            \ModuleManager::uninstallModule($module);
        }
    }
    foreach ($new_modules as $module) {
        if (!in_array($module, array('home', 'users')) && isset($need_modules[$module])) {
            \ModuleManager::installModule($module);
        }
    }

    $Viewer = new \Viewer_Manager(['template_path' => ROOT . '/install/template/html/', 'layout' => false]);
    $output = $Viewer->parseTemplate('step3.html.twig',  array());
    echo($output);
} else {
    header('Location: step2.php');
    die();
}
