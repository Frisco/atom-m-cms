<?php
include_once '../sys/boot.php';

@ini_set('display_errors', 0);
@ini_set('default_socket_timeout', 5);

// номер версии отправляется ТОЛЬКО в целях ведения внутренней статистики
$new_ver = file_get_contents('https://atom-m.modos189.ru/last.php?v=' . ATOM_VERSION);
if (!empty($new_ver)) {
    $new_ver = h($new_ver);
    if ($new_ver <= ATOM_VERSION) {
        echo 'Вы используете последнюю версию Atom-M ' . trim(ATOM_VERSION);
    } else {
        echo '<a href="https://atom-m.modos189.ru">Рекомендуем обновить до Atom-M ' . $new_ver . '</a>';
    }
} else {
    echo 'Не удалось узнать';
}
