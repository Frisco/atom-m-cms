<?php
@ini_set('display_errors', 0);


// set chmod
// Если являемся владельцем, то ограничиваем доступ с 777 до 755
function setChMod($path, $mode = 0755, $recursive = true) {
    clearstatcache();
    $flag = true;
    if (file_exists($path) && is_dir($path) && $recursive === true) {
        $child = glob($path . '/*');

        if (!empty($child)) {
            //recursive
            foreach ($child as $row) {
                if ($row != '.' && $row != '..') {
                    if (!setChMod($row)) {
                        $flag = false;
                    }
                }
            }
        }
        if (!@chmod($path, $mode) || $flag === false) return false;
        return true;

    } else if (file_exists($path)) {
        if (@chmod($path, $mode)) return true;
        return false;
    }
}


function checkWriteablePerms($path, $recursive = true) {
    if (file_exists($path) && is_dir($path) && $recursive === true) {
        $child = glob($path . '/*');

        $flag = true;
        if (!empty($child)) {
            //recursive
            foreach ($child as $row) {
                if ($row != '.' && $row != '..') {
                    if (!checkWriteablePerms($row)) {
                        $flag = false;
                    }
                }
            }
        }
        if (!@is_writeable($path) || $flag === false) return false;
        return true;
    } else {
        if (@is_writeable($path)) return true;
        return false;
    }
}

$out = '';
$flag = true;
$flag_update = true;
if (!setChMod('../', 0755, false) && !checkWriteablePerms('../', false)) {
    $out .= '<span style="color:#FF0000">/</span> - Права выставлены неверно (необходимо 777) <i>(for updates)</i><br />';
    $flag = false;
    $flag_update = false;
}
if (!setChMod('../admin', 0755, false) && !checkWriteablePerms('../admin', false)) {
    $out .= '<span style="color:#FF0000">/admin/</span> - Права выставлены неверно (необходимо 777)<br />';
    $flag = false;
}
if (!setChMod('../modules', 0755, false) && !checkWriteablePerms('../modules', false)) {
    $out .= '<span style="color:#FF0000">/modules/</span> - Права выставлены неверно (необходимо 777)<br />';
    $flag = false;
}
if (!setChMod('../data') && !checkWriteablePerms('../data')) {
    $out .= '<span style="color:#FF0000">/data/</span> - Права выставлены неверно (необходимо 777, рекурсивно)<br />';
    $flag = false;
}
if (!setChMod('../plugins', 0755, false) && !checkWriteablePerms('../plugins', false)) {
    $out .= '<span style="color:#FF0000">/plugins/</span> - Права выставлены неверно (необходимо 777)<br />';
    $flag = false;
}
if (!setChMod('../sys', 0755, false) && !checkWriteablePerms('../sys', false)) {
    $out .= '<span style="color:#FF0000">/sys/</span> - Права выставлены неверно (необходимо 777) <i>(for updates)</i><br />';
    $flag = false;
    $flag_update = false;
}
if (!setChMod('../core') && !checkWriteablePerms('../core')) {
    $out .= '<span style="color:#FF0000">/core/</span> - Права выставлены неверно (необходимо 777, рекурсивно)<br />';
    $flag = false;
}
if (!setChMod('../template') && !checkWriteablePerms('../template')) {
    $out .= '<span style="color:#FF0000">/template/</span> - Права выставлены неверно (необходимо 777, рекурсивно)<br />';
    $flag = false;
}
if (!setChMod('../sitemap.xml') && !checkWriteablePerms('../sitemap.xml')) {
    $out .= '<span style="color:#FF0000">/sitemap.xml</span> - Права выставлены неверно (необходимо 777)<br />';
    $flag = false;
}
if (!setChMod('../robots.txt') && !checkWriteablePerms('../robots.txt')) {
    $out .= '<span style="color:#FF0000">/robots.txt</span> - Права выставлены неверно (необходимо 777)<br />';
    $flag = false;
}

echo $out;
if ($flag === false) {
    echo '<span style="color:#E90E0E">Не удалось выставить права на все необходимые папки и файлы! Сделайте это в ручную.</span><br />';
    if ($flag_update === false) {
        echo '<br><span style="color:#D90000">Установку прав доступа на каталоги с описанием "<i>(for updates)</i>" можно пропустить, но это помешает использовать автоматическое обновление CMS.</span><br />';
    }
} else {
    echo '<span style="color:#46B100">Права на все необходимые файлы установлены верно!</span><br />';
}


?>
