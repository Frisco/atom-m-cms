<?php

if (basename(dirname(__FILE__)) != "install") {
    header('Location: /');
    die();
}

include_once '../sys/boot.php';
\ModuleManager::installModule('users'); // TODO: Necessary module "Users"

$installed_modules = \ModuleManager::getInstalledModules();
$new_modules = \ModuleManager::getNewModules();
$modules = array_merge($installed_modules, $new_modules);

$Viewer = new \Viewer_Manager(['template_path' => ROOT . '/install/template/html/', 'layout' => false]);
$output = $Viewer->parseTemplate(
    'step2.html.twig',
    array(
        'modules' => $modules,
        'installed_modules' => $installed_modules
    )
);

echo($output);
