<?php
/**
* @project    Atom-M CMS
* @package    Files library
* @url        https://atom-m.modos189.ru
*/



/**
 * Check and download attached avatar
 */
function downloadAvatar($module, $tmp_key) {
    if (!empty($_FILES['avatar']['name'])) {
        touchDir(ROOT . '/core/tmp/images/', 0755);

        $path = ROOT . '/core/tmp/images/' . $tmp_key . '.jpg';
        if (!isImageFile($_FILES['avatar'])) {
            $errors = '<li>' . __('Wrong avatar') . '</li>' . "\n";
            return $errors;
        }
        if ($_FILES['avatar']['size'] > \Config::read('max_avatar_size', $module)) {
            $errors = '<li>' . sprintf(__('Avatar is very big'), getSimpleFileSize(\Config::read('max_avatar_size', $module))) . '</li>' . "\n";
            return $errors;
        }
        if (move_uploaded_file($_FILES['avatar']['tmp_name'], $path)) {
            chmod($path, 0644);
            $AtmImg = new \AtmImg;
            @$sizes = $AtmImg->resampleImage($path, $path, \Config::read('max_avatar_resolution', $module));
            if (!$sizes) {
                @unlink($path);
                $errors = '<li>' . __('Some error in avatar') . '</li>' . "\n";
                return $errors;
            }
        } else {
            $errors = '<li>' . __('Some error in avatar') . '</li>' . "\n";
            return $errors;
        }
    }

    return null;
}



/**
 * Create secure and allowed filename.
 * Check to dublicate;
 *
 * @param string $filename
 * @param string $dirToCheck - dirrectory to check by dublicate
 * @return string
 */
function getSecureFilename($filename, $post_id, $i) {

    // Извлекаем из имени файла расширение
    $ext = strrchr($filename, ".");

    // Если имя файла содержит не только литиницу и: '.', '-', '_' то "очищаем имя от примесей"
    if (preg_match("/(^[a-zA-Z0-9]+([a-zA-Z\_0-9\.-]*))$/" , $filename)==NULL) {
        $atmurl = new AtmUrl;
        $filename = $atmurl->translit($filename);
        $filename = strtolower(preg_replace('#[^a-zA-Z\_0-9\.-]#i', '_', $filename));
        $filename = preg_replace('/(_)+/', '_', $filename);
        $filename = preg_replace('/^(_)/', '', $filename);
        $filename = preg_replace('/(_)$/', '', $filename);
    }


    // Формируем название файла
    if (!isPermittedFile($ext)) {
        //если расширение запрещенное, то заменяем его на .txt
        $filename = substr($filename, 0, strrpos($filename, '.', 0));
        $filename = ($filename) ? $filename : 'noname';
        $file = $post_id . '-' . $i . '_' . $filename . '.txt';
    } else
        $file = $post_id . '-' . $i . '_' . $filename;


    return $file;
}

/**
* Выясняет изображение ли файл
*
* param string|array $file - путь до файла либо массив $_FILES[$field_name]
*/
function isImageFile($file) {

    // Types of images
    $__fileslib__allowed_types = array('image/jpeg','image/jpg','image/gif','image/png', 'image/gif', 'image/pjpeg', 'image/tiff', 'image/vnd.microsoft.icon', 'image/x-icon', 'image/bmp', 'image/vnd.wap.wbmp');
    // Images extensions
    $__fileslib__img_extentions = array('.png','.jpg','.gif','.jpeg','.tiff', '.ico', '.bmp', '.wbmp');
    
    if (is_string($file))
        $file = array("tmp_name" => $file);
    
    $file = array_merge(array(
        "name"=> false,
        "tmp_name"=> false,
        "type" => false
    ), $file);
    
    if (empty($file['name']) && !is_string($file['name']))
        $file['name'] = $file['tmp_name'];
    
    // Простейшее отбрасывание файлов, не являющихся изображением
    if(($file['type'] !== false) and (strpos($file['type'],'image') === false)) {
        return false;
    }
    
    // Серьезная проверка "изображение ли"
    // Получение mime-type файла
    $imageinfo = @getimagesize($file['tmp_name']);
    // Извлекаем из имени файла расширение
    $ext = strrchr($file['name'], ".");
    $is_image = isset($imageinfo) && is_array($imageinfo) && in_array($imageinfo['mime'], $__fileslib__allowed_types);
    if (!empty($ext)) $is_image = $is_image && in_array(strtolower($ext), $__fileslib__img_extentions);
    return $is_image;
}

// Проверка на то, что файл является исполняемым(по расширению)
function isPermittedFile($ext) {
    // Wrong extention for download files
    $__fileslib__deny_extentions = array('.php', '.phtml', '.phps', '.phar', '.php3', '.php4', '.php5', '.html', '.htm', '.pl', '.js', '.htaccess', '.run', '.sh', '.bash', '.py');
    return !(empty($ext) || in_array(strtolower($ext), $__fileslib__deny_extentions));
}


function user_download_file($module, $file = null, $mimetype = 'application/octet-stream') {

    $error = null;
    //turn access
    \ACL::turnUser(array($module, 'download_files'),true);

    if (empty($file))
        return __('File not found');
    
    $path = ROOT . getFilePath($file, $module);
    
    if (!file_exists($path))
        return __('File not found');
    $from = 0;
    $size = filesize($path);
    $to = $size;
    if (isset($_SERVER['HTTP_RANGE'])) {
        if (preg_match('#bytes=-([0-9]*)#', $_SERVER['HTTP_RANGE'], $range)) {// если указан отрезок от конца файла
            $from = $size - $range[1];
            $to = $size;
        } elseif (preg_match('#bytes=([0-9]*)-#', $_SERVER['HTTP_RANGE'], $range)) {// если указана только начальная метка
            $from = $range[1];
            $to = $size;
        } elseif (preg_match('#bytes=([0-9]*)-([0-9]*)#', $_SERVER['HTTP_RANGE'], $range)) {// если указан отрезок файла
            $from = $range[1];
            $to = $range[2];
        }
        header('HTTP/1.1 206 Partial Content');

        $cr = 'Content-Range: bytes ' . $from . '-' . $to . '/' . $size;
    } else
        header('HTTP/1.1 200 Ok');

    $etag = md5($path);
    $etag = substr($etag, 0, 8) . '-' . substr($etag, 8, 7) . '-' . substr($etag, 15, 8);
    header('ETag: "' . $etag . '"');
    header('Accept-Ranges: bytes');
    header('Content-Length: ' . ($to - $from));
    if (isset($cr))
        header($cr);
    header('Connection: close');
    header('Content-Type: ' . $mimetype);
    header('Last-Modified: ' . gmdate('r', filemtime($path)));
    header("Last-Modified: " . gmdate("D, d M Y H:i:s", filemtime($path)) . " GMT");
    header("Expires: " . gmdate("D, d M Y H:i:s", time() + 3600) . " GMT");
    $f = fopen($path, 'rb');


    if (preg_match('#^image/#', $mimetype))
        header('Content-Disposition: filename="' . substr($file, strpos($file, '_', 0)+1) . '";');
    else
        header('Content-Disposition: attachment; filename="' . substr($file, strpos($file, '_', 0)+1) . '";');

    fseek($f, $from, SEEK_SET);
    $size = $to;
    $downloaded = 0;
    while (!feof($f) and ($downloaded < $size)) {
        $block = min(1024 * 8, $size - $downloaded);
        echo fread($f, $block);
        $downloaded += $block;
        flush();
    }
    fclose($f);
}

/**
 * Format file size from bytes to K|M|G
 *
 * @param int $size
 * @return string - simple size with letter
 */
function getSimpleFileSize($size) {
    $size = intval($size);
    if (empty($size)) return '0 B';

    if (\Config::read('IEC60027-2')==1) {
        $ext = array('B', 'KiB', 'MiB', 'GiB');
    } else {
        $ext = array('B', 'KB', 'MB', 'GB');
    }
    $i = 0;

    while (($size / 1024) > 1) {
        $size = $size / 1024;
        $i++;
    }


    $size = round($size, 2) . ' ' . $ext[$i];
    return $size;
}


/**
 * Similar to copy
 * @Recursive
 */
function copyr($source, $dest)
{
    // Simple copy for a file
    if (is_file($source)) {
        return copy($source, $dest);
    }

    // Make destination directory
    if (!is_dir($dest)) {
        mkdir($dest);
    }

    // If the source is a symlink
    if (is_link($source)) {
        $link_dest = readlink($source);
        return symlink($link_dest, $dest);
    }

    // Loop through the folder
    $dir = dir($source);
    while (false !== $entry = $dir->read()) {
        // Skip pointers
        if ($entry == '.' || $entry == '..') {
            continue;
        }

        // Deep copy directories
        if ($dest !== "$source/$entry") {
            copyr("$source/$entry", "$dest/$entry");
        }
    }

    // Clean up
    $dir->close();
    return true;
}

/**
 * Find all files in directory
 * @Recursive
 */
function getDirFiles($path) {
    $ret = array();
    $dir_iterator = new RecursiveDirectoryIterator($path);
    $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);

    foreach ($iterator as $file) {
        if($file->isFile()) $ret[] = str_replace(ROOT, '', (string)$file);
    }

    return $ret;
}


function _unlink($path, $ht = false) {
    if(is_file($path)) return unlink($path);

    if(!is_dir($path)) return;
    $dh = opendir($path);
    if ($dh === false) return;
    while (false !== ($file = readdir($dh))) {
        if($file == '.' || $file == '..' || ($file=='.htaccess' && $ht == true)) continue;
        _unlink($path."/".$file);
    }
    closedir($dh);

    return @rmdir($path);
}

// Сохраняет массив данныйх в файл в интерпретируемом формате
function save_export_file($data,$path,$method = 'return') {
    if ($fopen=@fopen($path, 'w')) {

        // Получаем строчное представление массива
        $export = var_export($data, true);
        // Убираем перенос перед array (
        $export = preg_replace('#=>[\s]+array \(#is', '=> array(', $export);
        // Убираем лишний пробел внутри array()
        $export = preg_replace('#array\([\s]+\)#is', 'array()', $export);

        // Записываем в файл
        fputs($fopen, '<?php '."\n".$method.' '.$export."\n".'?>');
        @fclose($fopen);
        return true;
    } else {
        return false;
    }
}



/**
* touch and create dir
*/
function touchDir($path, $chmod = 0777) {
    if (!file_exists($path)) {
        mkdir($path, $chmod, true);
        chmod($path, $chmod);
    }
    return true;
}

// without ROOT or WWW_ROOT
function getFilePath($filename, $module) {
    // Images extensions
    $__fileslib__img_extentions = array('.png','.jpg','.gif','.jpeg','.tiff', '.ico', '.bmp', '.wbmp');

    // Извлекаем из имени файла расширение
    $ext = strrchr($filename, ".");
    if (!empty($ext) && in_array(strtolower($ext), $__fileslib__img_extentions))
        return '/data/images/' . $module . '/' . $filename;
    return '/data/files/' . $module . '/' . $filename;
}
