<?php
/*
 * The Proxy that allows us to use Atom-M functions in Twig Templates
 */
class TwigProxy {
    protected $returns_closure = array();
    protected $context = array();

    function __construct($context) {
        $this->context = $context;
    }

    public function __call($function, $arguments) {
        return $this->getValue($this->context, $function, $arguments);
    }

    private function getValue($context, $function, $arguments) {

        // Запрос на получение значение метки
        if (array_key_exists($function, $context)) {

            $context = $context[$function];

            // Если мы имеем дело с анонимной функцией,
            // то запускаем её для получения значения,
            // само значение сохраняем в контекст,
            // чтобы не запускать функцию несколько раз.
            if (is_object($context) && is_callable($context)) {
                $id = spl_object_hash($context);
                if (!array_key_exists($id, $this->returns_closure)) {

                    $raw = $context($arguments);
                    $data = array();

                    // Обработка анонимных функций, если имеются
                    if (is_array($raw)) {
                        foreach ($raw as $k => $item) {
                            $data[$k] = $this->getValue($raw, $k, $arguments);
                        }
                    } else {
                        $data = $raw;
                    }

                    $this->returns_closure[$id] = $data;
                }
                return $this->returns_closure[$id];

            // Иначе просто вывод значения
            } else {
                return $this->getAllValues($context);
            }

        // вызов функции
        } else {

            if (!function_exists($function)) {
                return NULL;
            }
            return call_user_func_array($function, $arguments);

        }

    }

    private function getAllValues($context) {

        if (is_array($context)) {

            $data = array();
            foreach ($context as $k => $item) {
                $data[$k] = $this->getAllValues($item);
            }
            return $data;
        }

        if (is_object($context) && is_callable($context)) {

            $id = spl_object_hash($context);
            if (!array_key_exists($id, $this->returns_closure)) {

                $raw = $context();
                $data = array();

                // Обработка анонимных функций, если имеются
                if (is_array($raw)) {
                    foreach ($raw as $k => $item) {
                        $data[$k] = $this->getAllValues($item);
                    }
                } else {
                    $data = $raw;
                }

                $this->returns_closure[$id] = $data;
            }
            return $this->returns_closure[$id];

        }

        if (is_object($context)) {

            $data = array();
            $keys = $context->getListKeys();
            if (count($keys) > 0) {
                foreach ($keys as $key) {
                    $getter = 'get' . ucfirst(strtolower($key));

                    $data[$key] = $this->getAllValues($context->$getter());
                }
                return $data;
            } else {
                error_log("Please check method getListKeys() in ".get_class($context), 0);
            }

        }

        return $context;

    }
}
