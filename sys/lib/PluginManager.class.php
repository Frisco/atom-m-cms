<?php
/**
* @project    Atom-M CMS
* @package    Plugins Class
* @url        https://atom-m.modos189.ru
*/


class PluginManager {
    
    private static $errors = '';
    
    private static $pluginsPath = 'plugins';
    private static $tempPath = 'core/tmp/';
    
    // Обязательные файлы
    private static $indexFile = 'index.php';
    private static $configFile = 'config.json';
    // Необязательные файлы
    private static $settingsFile = 'settings.php';
    private static $versionFile = '.version';
    
    public function __construct() {
        
    }


    /** Путь к каталогу плагина */
    public static function getPluginPath($plugin) {
        return R . self::$pluginsPath . DS . $plugin . DS;
    }


    /** Проверяет на существование плагин */
    public static function checkPlugin($plugin) {
        $plugin_path = self::getPluginPath($plugin);
        return (file_exists($plugin_path . self::$indexFile) && file_exists($plugin_path . self::$configFile));
    }


    /** Возвращает массив неустановленных, но готовых к установке плагинов */
    public static function getNewPlugins() {
        $new_plugins = array();

        $plugin_paths = glob(R . self::$pluginsPath . DS . '*', GLOB_ONLYDIR);
        foreach ($plugin_paths as $plugin_path) {
            $plugin = basename($plugin_path);
            if (!in_array($plugin, $new_plugins) && self::checkPlugin($plugin) && !self::checkInstallPlugin($plugin)) {
                $new_plugins[] = $plugin;
            }
        }
        return $new_plugins;
    }


    /** Возвращает массив установленных плагинов */
    public static function getInstalledPlugins() {
        return \Config::read('installed_plugins');
    }


    /** Сохраняет массив установленных плагинов */
    public static function setInstalledPlugins($installed_plugins) {
        $config = \Config::read('all');
        $config['installed_plugins'] = $installed_plugins;
        \Config::write($config);
        
        clearstatcache();
        if (function_exists('opcache_reset')) {
            opcache_reset();
        }
    }


    /** Возвращает путь к файлу плагина */
    public static function getIndexFile($plugin) {
        return self::getPluginPath($plugin) . self::$indexFile;
    }


    /** Возвращает путь к файлу настроек плагина */
    public static function getConfigFile($plugin) {
        return self::getPluginPath($plugin) . self::$configFile;
    }


    /** Возвращает путь к файлу визуализации настроек плагина */
    public static function getSettingsFile($plugin) {
        return self::getPluginPath($plugin) . self::$settingsFile;
    }


    /** Возвращает последние ошибки, связанные с установкой плагина */
    public static function getErrors() {
        return self::$errors;
    }


    public static function getPluginFiles($plugin) {
        return getDirFiles(self::getPluginPath($plugin));
    }


    /** Проверяет был ли установлен плагин */
    public static function checkInstallPlugin($plugin) {
        $installed_plugins = self::getInstalledPlugins();
        return (is_array($installed_plugins) && in_array($plugin, $installed_plugins));
    }


    /** Устанавливает плагин */
    public static function extractPlugin($filename, $plugin = false) {
        self::$errors = '';
        $src = R . self::$tempPath . $filename;
        $dest = R . self::$tempPath . 'install_plugin/';

        _unlink($dest);
        Zip::extractZip($src, $dest);
        if (!file_exists($dest)) {
            self::$errors = __('Some error occurred');
            return false;
        }

        $tmp_paths = glob($dest . '*', GLOB_ONLYDIR);
        if (!count($tmp_paths)) {
            self::$errors = __('Some error occurred');
            return false;
        }
        $plugin_path = '';
        foreach($tmp_paths as $tmp_path) {
            if (file_exists($tmp_path . DS . self::$indexFile) && file_exists($tmp_path . DS . self::$configFile)) {
                $plugin_path = $tmp_path;
                break;
            }
        }
        $plugin = $plugin ? $plugin : basename($plugin_path);
        $result = (!empty($plugin) && copyr($plugin_path, self::getPluginPath($plugin)));

        _unlink($src);
        _unlink($dest);
        
        if (!$result) {
            self::$errors = __('Some error occurred');
            return false;
        }
        return $plugin;
    }


    /** Устанавливает плагин */
    public static function installPlugin($plugin, $version=Null) {
        error_log('install plugin '.$plugin);
        self::$errors = '';
        if (self::checkPlugin($plugin)) {
            $config = json_decode(file_get_contents(self::getConfigFile($plugin)), true);

            include_once self::getIndexFile($plugin);

            $class_name = isset($config['className']) ? $config['className'] : $plugin;

            if (!class_exists($class_name)) {
                self::$errors = __('Some error occurred');
                return false;
            }
            $obj = new $class_name(Null);
            if (method_exists($obj, 'install')) {
                if ($obj->install() == false) {
                    self::$errors = __('Some error occurred');
                    return false;
                }
            }

            $config['active'] = 1;
            file_put_contents(self::getConfigFile($plugin), json_encode($config, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
            
            if ($version !== null) {
                file_put_contents(self::getPluginPath($plugin) . self::$versionFile, $version);
            }
            
            $installed_plugins = self::getInstalledPlugins();
            if (is_array($installed_plugins)) {
                $installed_plugins[] = $plugin;
                self::setInstalledPlugins(array_unique($installed_plugins));
            }
        }
        return true;
    }


    /** Удаляет плагин */
    public static function uninstallPlugin($plugin, $purge=true) {
        // Если модуль существует и является плагином
        if (self::checkInstallPlugin($plugin)) {
            if (self::checkPlugin($plugin)) {
                $config = json_decode(file_get_contents(self::getConfigFile($plugin)), true);

                include_once self::getIndexFile($plugin);

                $class_name = isset($config['className']) ? $config['className'] : $plugin;

                if (!class_exists($class_name)) {
                    self::$errors = __('Some error occurred');
                    return false;
                }
                
                // полное удаление плагина
                if ($purge) {
                    $obj = new $class_name(null);
                    if (method_exists($obj, 'uninstall')) {
                        if ($obj->uninstall() == false) {
                            self::$errors = __('Some error occurred');
                            return false;
                        }
                    }
                }

                _unlink(self::getPluginPath($plugin));
            }
            $installed_plugins = self::getInstalledPlugins();
            if (is_array($installed_plugins)) {
                foreach (array_keys($installed_plugins, $plugin) as $key) {
                    unset($installed_plugins[$key]);
                }
                self::setInstalledPlugins(array_unique($installed_plugins));
            }
        }
        return true;
    }


    /** Загрузка плагина по URL */
    public static function downloadPlugin($url, $plugin = false) {
        self::$errors = '';
        $headers = get_headers($url, 1);
        $matches = array();
        if (!empty($headers['Content-Disposition'])) {
            preg_match('#filename="(.*)"#iU', $headers['Content-Disposition'], $matches);
        }
        $filename = (isset($matches[1])) ? $matches[1] : basename($url);

        $ext = strrchr($filename, '.');
        if (strtolower($ext) !== '.zip') {
            self::$errors = sprintf(__('Wrong file format'),$filename);
            return false;
        }

        if (copy($url, R . self::$tempPath . $filename)) {
            return self::extractPlugin($filename, $plugin);
        } else {
            self::$errors = __('Some error occurred');
            return false;
        }
    }


    /** Загрузка плагина из формы */
    public static function receivePlugin($field) {
        self::$errors = '';
        if (!isset($_FILES[$field]['name']) || empty($_FILES[$field]['name'])) {
            self::$errors = __('File not found');
            return false;
        }
        $filename = $_FILES[$field]['name'];

        $ext = strrchr($filename, '.');
        if (strtolower($ext) !== '.zip') {
            self::$errors = sprintf(__('Wrong file format'), $filename);
            return false;
        }

        if (move_uploaded_file($_FILES[$field]['tmp_name'], R . self::$tempPath . $filename)) {
            return self::extractPlugin($filename);
        } else {
            self::$errors = __('Some error occurred');
            return false;
        }
    }


    /** Установка состояния плагина */
    private static function setStatusPlugin($plugin, $active) {
        if (file_exists(self::getConfigFile($plugin))) {
            $config = json_decode(file_get_contents(self::getConfigFile($plugin)), true);
            $config['active'] = $active;
            file_put_contents(self::getConfigFile($plugin), json_encode($config, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
            return true;
        }
        return false;
    }


    /** Включение плагина */
    public static function enablePlugin($plugin) {
        return self::setStatusPlugin($plugin, 1);
    }


    /** Отключение плагина */
    public static function disablePlugin($plugin) {
        return self::setStatusPlugin($plugin, 0);
    }

    /** Возвращает версию плагина */
    public static function getPluginVersion($plugin) {
        $path = self::getPluginPath($plugin) . self::$versionFile;
        if (file_exists($path)) {
            return file_get_contents($path);
        }
        return null;
    }
}
