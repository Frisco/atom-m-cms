<?php
/**
* @project    Atom-M CMS
* @package    Attaches Trait
* @url        https://atom-m.modos189.ru
*/

namespace Traits;

trait Attaches {
    /**
     * Check attached files
     *
     * @param bool $onlyimg (если разрешено загружать только изображения)
     *
     * @return null|string
     */

    protected function checkAttaches($onlyimg = null) {
        $error = null;

        // Получаем максимальное количество допустимых прикреплений
        $max_attach = \Config::read('max_attaches', $this->module);
        if (empty($max_attach) || !is_numeric($max_attach)) $max_attach = 5;

        // Получаем максимально возможный размер файла
        $max_attach_size = \Config::read('max_attaches_size', $this->module);
        if (empty($max_attach_size) || !is_numeric($max_attach_size)) $max_attach_size = 1048576;

        // Если настройка не задана принудительно, то получаем её сами
        if (empty($onlyimg))
            $onlyimg = intval(\Config::read('onlyimg_attaches', $this->module));

        for ($i = 1; $i <= $max_attach; $i++) {
                // Формируем имя формы с файлом
                $attach_name = 'attach' . $i;
                // Находим прикрепленный файл
                if (!empty($_FILES[$attach_name]['name'])) {
                    $name = $_FILES[$attach_name]['name'];
                    // Если точку не находим, то возвращаем запрет на такой файл.
                    if (!strrpos($name, '.', 0))
                        $error .= '<li>' . sprintf(__('Wrong file format'), '(' . $name . ')') .'</li>' . "\n";
                    // Проверяем файл на максимальный размер
                    if ($_FILES[$attach_name]['size'] > $max_attach_size)
                        $error .= '<li>' . sprintf(__('Very big file'), $name, getSimpleFileSize($max_attach_size)) . '</li>' . "\n";
                    // Если разрешено загружать только изображения проверяем и это
                    if ($onlyimg and !isImageFile($_FILES[$attach_name]))
                        $error .= '<li>' . sprintf(__('Wrong file format'), '(' . $name . ')') .'</li>' . "\n";
                }
        }
        return $error;
    }


    /**
     * Download attached files
     *
     * @param int $entity_id (id material or id post)
     * @param bool $unlink удалять ли прикрепления, если нужно
     *
     * @return null|string
     */
    protected function downloadAttaches($entity_id, $unlink = false) {

        $error = null;
        if (empty($entity_id) || !is_numeric($entity_id)) return '<li>' . __('Some error occurred') . '</li>' . "\n";

        // Получаем максимальное количество допустимых прикреплений
        $max_attach = \Config::read('max_attaches', $this->module);
        if (empty($max_attach) || !is_numeric($max_attach)) $max_attach = 5;

        for ($i = 1; $i <= $max_attach; $i++) {

            // Формируем имя формы с файлом
            $attach_name = 'attach' . $i;

            // при редактировании, удаляет замененные файлы или у которых стоит глочка "удалить".
            if ($unlink and (!empty($_POST['unlink' . $i]) or !empty($_FILES[$attach_name]['name'])))
                $this->deleteAttach($entity_id, $i);

            // Находим прикрепленный файл
            if (!empty($_FILES[$attach_name]['name'])) {

                $files_dir = ROOT . '/data/files/' . $this->module . '/';

                // Формируем имя файла
                $filename = getSecureFilename($_FILES[$attach_name]['name'], $entity_id, $i);

                // Узнаем, изображение ли мы загружаем
                $is_image = isImageFile($_FILES[$attach_name]) ? 1 : 0;
                if ($is_image)
                    $files_dir = ROOT . '/data/images/' . $this->module . '/';

                // Перемещаем файл из временной директории сервера в директорию files
                if (!file_exists($files_dir)) mkdir($files_dir,0766);
                if (move_uploaded_file($_FILES[$attach_name]['tmp_name'], $files_dir . $filename)) {
                    // Если изображение, накладываем ватермарк
                    if ($is_image) {
                        $watermark_path = ROOT . '/data/img/' . (\Config::read('watermark_type') == '1' ? 'watermark_text.png' : \Config::read('watermark_img'));
                        if (\Config::read('use_watermarks') && !empty($watermark_path) && file_exists($watermark_path)) {
                            $waterObj = new \AtmImg;
                            $save_path = $files_dir . $filename;
                            $waterObj->createWaterMark($save_path, $watermark_path);
                        }
                    }

                    // если возможно выставляем доступ к файлу.
                    chmod($files_dir . $filename, 0644);

                    // Формируем данные о файле
                    $attach_file_data = array(
                        'user_id'       => \UserAuth::getField('id'),
                        'attach_number' => $i,
                        'filename'      => $filename,
                        'size'          => $_FILES[$attach_name]['size'],
                        'date'          => (new \DateTime())->format('Y-m-d H:i:s'),
                        'is_image'      => $is_image ? '1' : '0',
                        'entity_id'     => $entity_id,
                        'module'        => $this->module,
                    );

                    // Сохраняем данные о файле
                    $className = 'ORM\\AttachesEntity';
                    $entity = new $className($attach_file_data);
                    // Если при сохранении в БД произошла ошибка, то выводим сообщение и удаляем файл, чтобы не занимал места.
                    if ($entity->save() == NULL) {
                        $error .= '<li>' . sprintf(__('File is not load'), $_FILES[$attach_name]['name']) . '</li>' . "\n";
                        _unlink($files_dir . $filename);
                    }

                } else
                    $error .= '<li>' . sprintf(__('File is not load'), $_FILES[$attach_name]['name']) . '</li>' . "\n";
            }
        }

        return $error;
    }


    /**
     * Delete attached file
     *
     * @param int $entity_id (id material or id post)
     * @param int $attachNum номер прикрепленного файла
     *
     * @return bool
     */
    protected function deleteAttach($entity_id, $attachNum) {
        $attachModel = \OrmManager::getModelInstance('Attaches');
        $attach = $attachModel->getFirst(array(
            'entity_id'     => $entity_id,
            'attach_number' => $attachNum,
            'module'        => $this->module,
        ), array());

        if ($attach) {
            $attach->delete();
        }
        return true;
    }
}
