<?php
/**
* @project    Atom-M CMS
* @package    Additional Fields
* @url        https://atom-m.modos189.ru
*/

namespace Traits;

trait AdditionalFields {

    private function getAllAdditionalFields($module) {
        if ($this->Cache->check($module . '_additional_fields')) {
            $addFields = unserialize($this->Cache->read($module . '_additional_fields'));
        } else {
            // Получение свойств полей
            $fieldsModel = \OrmManager::getModelInstance('AddFields');
            $addFields = $fieldsModel->getCollection(array('module' => $module));
            
            $this->Cache->write(serialize($addFields), $module . '_additional_fields', array('module_' . $module, 'additional_fields'));
        }
        return $addFields;
    }
    

    /**
     * Обьединяет данные из sql запроса с выводом метки селекта
     *
     * @param array $records
     * @return mixed
     */
    public function mergeAdditionalFields($records, $module = null)
    {
        if (empty($module)) {
            $module = $this->module;
        }
        
        $addFields = $this->getAllAdditionalFields($module);

        if (!empty($addFields) && is_array($addFields)) {
            foreach ($records as $entity) {
                foreach ($addFields as $addField) {
                    $id = $addField->getField_id();
                    $type = $addField->getType();
                    
                    // Если это форма с выбором из вариантов, то нужно вывести варианты в отдельную метку.
                    if ($type == 'select') {
                        $params = $addField->getParams();
                        if (!empty($params))
                            $params = unserialize($params);
                        else
                            throw new Exception("Error getting parameters \"AddField\"(id:$id) the type SELECT");
                        
                        $field_selects_marker = 'add_field_select_' . $id;
                        $entity->$field_selects_marker = $params['values'];
                    }
                    
                    

                }
            }
        }
        return $records;
    }



    /**
     * Возвращает массив доп. полей без заполнения их данными
     * Для формы добавления материала
     */
    public function getAdditionalFields($module = null)
    {
        if (empty($module)) {
            $module = $this->module;
        }
        
        $addFields = $this->getAllAdditionalFields($module);

        $output = array();

        if (!empty($addFields)) {
            foreach($addFields as $field) {
                $id = $field->getField_id();
                $type = $field->getType();
                $params = (!empty($field->getParams())) ? unserialize($field->getParams()) : array();

                $value = '';

                switch ($type) {
                    case 'checkbox':
                        $value = 1;
                        break;
                    case 'select':
                        $output['add_field_select_' . $id] = $params['values'];
                        break;
                }
                $output['add_field_' . $id] = $value;
            }
        }
        return $output;
    }



    /**
     * Проверяет значения полей
     */
    public function checkAdditionalFields($module = null) {
        if (empty($module)) {
            $module = $this->module;
        }
        
        $addFields = $this->getAllAdditionalFields($module);

        $CheckedAddFields = array();
        $error = null;
        
        
        if (!empty($addFields)) {
            foreach($addFields as $field) {
                $params = (!empty($field->getParams())) ? unserialize($field->getParams()) : array();
                
                $field_name = 'add_field_' . $field->getField_id();
                $content = null;
                
                $value = isset($_POST[$field_name]) ? $_POST[$field_name] : null;
                
                if (!empty($params['required']) && empty($value))
                    $error .= '<li>'.sprintf(__('Empty field "param"'), $field->getLabel()).'</li>';
                if (!empty($value) && mb_strlen($value) > (int)$field->getSize())
                    $error .= '<li>'.sprintf(__('Very big "param"'), $field->getLabel(), $field->getSize()).'</li>';

                switch ($field->getType()) {
                    case 'text':
                        $content = $value;

                        if (!empty($params['pattern']) && !empty($array['content'])) {
                            
                            $pattern = $params['pattern'];
                            if (substr($pattern, 0, 2) == 'V_')
                                $pattern = constant($pattern);

                            if (!Validate::cha_val($value,$pattern))
                                $error .= '<li>'.sprintf(__('Wrong chars in field "param"'), $field->getLabel()).'</li>';

                        }

                        break;
                    case 'checkbox':
                        if (!empty($value))
                            $content = 1;
                        else
                            $content = 0;
                        break;
                    case 'select':
                        if ($value !== null)
                            $content = $value;
                        break;
                }
                $CheckedAddFields[$field_name] = $content;
            }
        }

        return (!empty($error)) ? $error : $CheckedAddFields;
    }
}
