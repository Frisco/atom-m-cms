<?php
/**
* @project    Atom-M CMS
* @package    Attaches Model
* @url        https://atom-m.modos189.ru
*/


namespace ORM;

class AttachesModel extends \OrmModel
{

    public $Table = 'attaches';

}