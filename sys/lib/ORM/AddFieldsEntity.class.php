<?php
/**
* @project    Atom-M CMS
* @package    AddFields Entity
* @url        https://atom-m.modos189.ru
*/


namespace ORM;

class AddFieldsEntity extends \OrmEntity
{
    protected $id;
    protected $field_id;
    protected $module;
    protected $type;
    protected $name;
    protected $label;
    protected $size;
    protected $params;
    protected $indexed;
    protected $content;


    public function getListKeys() {
        return array_keys(get_object_vars($this));
    }
}
