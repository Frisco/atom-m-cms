<?php
/**
* @project    Atom-M CMS
* @package    Footer html
* @url        https://atom-m.modos189.ru
*/
?>
        </div>
        </main>

        <footer class="page-footer no-margin">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Atom-M CMS</h5>
                <p class="grey-text text-lighten-4">Talk is cheap. Try our CMS.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text"><?php echo __('Helpful'); ?></h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="https://bitbucket.org/atom-m/cms/"><?php echo __('Repository'); ?></a></li>
                  <li><a class="grey-text text-lighten-3" href="https://bitbucket.org/atom-m/cms/wiki/Home"><?php echo __('Wiki'); ?></a></li>
                  <li><a class="grey-text text-lighten-3" href="https://bitbucket.org/atom-m/cms/issues?status=new&status=open"><?php echo __('Report an error'); ?></a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2015 <a class="grey-text text-lighten-3" href="https://atom-m.modos189.ru">Atom-M CMS</a>. All rights reserved.
            <a class="grey-text text-lighten-4 right modal-trigger" href="#Thanks"><?php echo __('Say Thanks'); ?></a>
            </div>
          </div>
        </footer>
        <!--Import materialize.js-->
        <script type="text/javascript" src="<?php echo WWW_ROOT ?>/admin/template/js/materialize.min.js"></script>
        <script type="text/javascript">$(".button-collapse[data-activates=\"mobile-navmenu\"]").sideNav();</script>
        
        <script type="text/javascript" src="<?php echo WWW_ROOT ?>/admin/js/drunya.lib.js"></script>
        <script type="text/javascript">
            <?php
                // Уведомления
                if (isset($_SESSION['message']) && count($_SESSION['message']) > 0) {
                    foreach($_SESSION['message'] as $k => $v) {
                        echo "Materialize.toast('". $v ."', 4000);\n";
                    }
                    unset($_SESSION['message']);
                }
            ?>
            $('.modal-trigger').leanModal();
        </script>
        
        
        
        <div id="Thanks" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4><?php echo __('Say Thanks'); ?></h4>
                <div class="center">
                    <iframe frameborder="0" allowtransparency="true" scrolling="no" src="https://money.yandex.ru/embed/donate.xml?account=410012462049443&quickpay=donate&payment-type-choice=on&default-sum=100&targets=%D0%A1%D0%BF%D0%B0%D1%81%D0%B8%D0%B1%D0%BE+%D0%BA%D0%BE%D0%BC%D0%B0%D0%BD%D0%B4%D0%B5+Atom-M+CMS+%D0%B7%D0%B0+%D0%B8%D1%85+%D0%B1%D0%B5%D1%81%D0%BA%D0%BE%D1%80%D1%8B%D1%81%D1%82%D0%BD%D1%8B%D0%B9+%D1%82%D1%80%D1%83%D0%B4&target-visibility=on&project-name=Atom-M+CMS&project-site=http%3A%2F%2Fatom-m.modos189.ru&button-text=01&successURL=" width="525" height="131"></iframe>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect btn-flat"><?php echo __('Cancel'); ?></a>
            </div>
        </div>
        
    </body>
</html>
