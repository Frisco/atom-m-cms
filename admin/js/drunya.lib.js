//OPEN Mobile MENU
	
function wind(){
  var height=(document.body.scrollHeight > document.body.offsetHeight)?document.body.scrollHeight:document.body.offsetHeight;


  if ($('#rightmenu').css('position') !== 'absolute') {
	height = height - 38;
	$('#rightmenu').css('min-height', height);
  } else {
	height = height - 121;
	$('#rightmenu, #wrapper, .crumbs, .rcrumbs').removeAttr("style");
  }
  $('#leftmenu').css('min-height', height);
}
window.onload=function() {wind();}
window.onresize=function() {wind();}


function save(prefix) {

	var inp = document.getElementById(prefix + '_inp').value;
	if (prefix == 'cat')
		var id_sec = document.getElementById(prefix + '_secId').value;
	else
		var id_sec = '';
	if (typeof inp == 'undefined' || typeof inp == '' || inp.length < 2) {
		alert('Слишком короткое название');
		return;
	} else {
		$.post('load_cat.php?ac=add', {title : inp, type: prefix, id_sec: id_sec}, function(data) { window.location.href = ''; });
		
	}
}
function _confirm() {
	return confirm('Вы уверены?');
}

function closeHelpPopup(id) {
	$('#'+id).fadeOut(400, function(){
		$('#'+id).remove();
	})
}




/* ****** TOP MENU ******* */
var drunya_menu = false;
var menu_item_over = false;
document.onclick = function() {
	if (menu_item_over == false) {
		drunya_menu = false;
		hideAll();
	}
}


function hideAll() {
	$('#topmenu > ul > li .sub').each(function(){
		$(this).slideUp('fast');
	});
}




/* ************  MENU BLOCK ************ */

/**
 * Change image when changing template
 */
 
 
function showScreenshot(root) {
    var template = $('#templateScreen').val(),
	     href = root + template + '/screenshot.png',
	     img = $('#screenshot');
	img.attr('src', href);
	img.parent('a.gallery').attr('href', href);
}